package com.example.ms_app;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class results extends AppCompatActivity {

    private XYPlot plot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        ArrayList<Number> left_score = new ArrayList<Number>();
        ArrayList<Number> right_score = new ArrayList<Number>();
        final ArrayList<Date> dates = new ArrayList<Date>();
        String str = "";
        SimpleDateFormat df = new SimpleDateFormat("MM/dd HH:mm");

        try {
            FileInputStream in = openFileInput("results");

            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line = br.readLine();
            String[] split = line.split("-");
            //str += "Date: " + split[0] + "Left Hand Average: " + split[1] + "Right Hand Average: " + split[1] + "\n";
            Log.d("total", line);
            dates.add(df.parse(split[0]));
            left_score.add(Integer.parseInt(split[1]));
            right_score.add(Integer.parseInt(split[2]));
            line = br.readLine();
            while (line != null) {
                Log.d("total", line);
                split = line.split("-");
                dates.add(df.parse(split[0]));
                left_score.add(Integer.parseInt(split[1]));
                right_score.add(Integer.parseInt(split[2]));
                line = br.readLine();
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        String arr[] = str.split("\n");
//        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.activity_listview, arr);
//        ListView listView = (ListView) findViewById(R.id.result_list);
//        listView.setAdapter(adapter);

        plot = (XYPlot) findViewById(R.id.plot);
        Log.d("left list", left_score.toString());
        XYSeries series1 = new SimpleXYSeries(left_score, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "left_hand");
        XYSeries series2 = new SimpleXYSeries(right_score, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "right_hand");

        LineAndPointFormatter series1Format = new LineAndPointFormatter(Color.BLUE, Color.BLUE, null, null);
        series1Format.setPointLabelFormatter(new PointLabelFormatter());

        LineAndPointFormatter series2Format = new LineAndPointFormatter(Color.RED, Color.RED, null, null);
        series1Format.setPointLabelFormatter(new PointLabelFormatter());


        plot.addSeries(series1, series1Format);
        plot.addSeries(series2, series2Format);

        plot.setDomainStep(StepMode.SUBDIVIDE, dates.size());
        plot.getGraph().setPaddingBottom(70);
        plot.getGraph().setPaddingLeft(70);

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            private SimpleDateFormat df = new SimpleDateFormat("MM/dd HH:mm");
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                int i = ((Number) obj).intValue();
                Date date = dates.get(i);
                return df.format(date, toAppendTo, pos);
            }


            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });

    }

}
