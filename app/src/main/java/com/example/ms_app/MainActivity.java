package com.example.ms_app;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.widget.Button;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button left_button;
    Button right_button;
    public static int trialLength = 10;
    public static int amountTrials = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        left_button = (Button) findViewById(R.id.left_button);
        left_button.setOnClickListener(this);
        right_button = (Button) findViewById(R.id.right_button);
        right_button.setOnClickListener(this);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Tapping Test");

        SharedPreferences spf = PreferenceManager.getDefaultSharedPreferences(this);
        String storeTrials = spf.getString(getString(R.string.key_trials), "1");
        amountTrials = Integer.parseInt(storeTrials);
        String storeLength = spf.getString(getString(R.string.key_length), "10");
        trialLength = Integer.parseInt(storeLength);
    }

    private void continueButtonClick(boolean right) {
        Intent intent = new Intent("ms_app.TappingGame");
        intent.putExtra("right_hand", right);
        intent.putExtra("trialLength", trialLength);
        intent.putExtra("amountTrials", amountTrials);
        startActivity(intent);
    }

    private void settingsButtonClick() {
        finish();
        Intent intent = new Intent(getApplicationContext(), activity_settings.class);  //"ms_app.settings");
        startActivity(intent);
    }

    private void resultButtonClick() {
        Intent intent = new Intent("ms_app.results");
        startActivity(intent);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                settingsButtonClick();
                return true;
            case R.id.results:
                resultButtonClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.left_button:
                continueButtonClick(false);
                break;
            case R.id.right_button:
                continueButtonClick(true);
                break;

        }
    }
}
