package com.example.ms_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.app.AlertDialog;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import cmsc436.tharri16.googlesheetshelper.CMSC436Sheet;

public class TappingGame extends AppCompatActivity {

    private static final int LIB_ACCOUNT_NAME_REQUEST_CODE = 336;
    private static final int LIB_AUTHORIZATION_REQUEST_CODE = 489;
    private static final int LIB_PERMISSION_REQUEST_CODE = 219;
    private static final int LIB_PLAY_SERVICES_REQUEST_CODE = 424;
    private int tapCount = 0;
    private int time_left;
    private int time_left_temp;
    private boolean right;
    private boolean runOtherHand = true;
    private int trialAmount;
    private int tempTrialAmount;
    private int tapAverageRight;
    private int tapAverageLeft;
    private TextView rightHand;
    private TextView leftHand;
    private TextView timer_text;
    private Button tapButton;
    File myFile;
    OutputStream outputStream;
    CMSC436Sheet sheet;

    @Override
    public void onRequestPermissionsResult (int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        sheet.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        sheet.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        right = getIntent().getExtras().getBoolean("right_hand");
        time_left = getIntent().getExtras().getInt("trialLength")+5;
        time_left_temp = time_left;
        trialAmount = getIntent().getExtras().getInt("amountTrials");
        setContentView(R.layout.activity_tapping_game);
        timer_text = (TextView) findViewById(R.id.timer);
        leftHand = (TextView) findViewById(R.id.textView6);
        rightHand = (TextView) findViewById(R.id.textView7);
        tapButton = (Button) findViewById(R.id.button);
        tempTrialAmount = trialAmount;
        tapAverageRight = 0;
        tapAverageLeft = 0;
        createFile();

        sheet = new CMSC436Sheet(new CMSC436Sheet.Host() {
            @Override
            public int getRequestCode(CMSC436Sheet.Action action) {
                switch (action) {
                    case REQUEST_ACCOUNT_NAME:
                        return LIB_ACCOUNT_NAME_REQUEST_CODE;
                    case REQUEST_AUTHORIZATION:
                        return LIB_AUTHORIZATION_REQUEST_CODE;
                    case REQUEST_PERMISSIONS:
                        return LIB_PERMISSION_REQUEST_CODE;
                    case REQUEST_PLAY_SERVICES:
                        return LIB_PLAY_SERVICES_REQUEST_CODE;
                    default:
                        return -1; // boo java doesn't know we exhausted the enum
                }
            }

            @Override
            public Activity getActivity() {
                return TappingGame.this;
            }

            @Override
            public void notifyFinished(Exception e) {
                if (e != null) {
                    throw new RuntimeException(e); // just to see the exception easily in logcat
                }

                Log.i(getClass().getSimpleName(), "Done");
            }
        }, "MSApp", getString(R.string.google_sheets_class_id));
    }

    private void createFile() {
        String FILENAME = "results";
        try {
            outputStream = openFileOutput(FILENAME, Context.MODE_APPEND);
            Log.d("file", "opened");
        } catch (Exception e) {
            File file = new File(FILENAME);
            try {
                file.createNewFile();
                Log.d("file", "created");
                outputStream = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }
    private void save() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd HH:mm");
        String formattedDate = df.format(c.getTime());
        String res = formattedDate + "-" + String.valueOf(tapAverageLeft) + "-" + String.valueOf(tapAverageRight) + "\n";
        try {
            outputStream.write(res.getBytes());
            outputStream.flush();
            Log.d("write", "output to file");
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void handleTap(View view) {
        runGame();
    }

    private void runGame(){
        if (tapCount == 0) {
            Runnable task = new Runnable() {
                @Override
                public void run() {
                    if (time_left_temp == 0) {
                        timer_text.setText(String.valueOf(time_left-5) + " seconds");
                        tapButton.setEnabled(false);
                        // cheat on not having proper button states by filtering the background color
                        tapButton.getBackground().setColorFilter(0x88880000, PorterDuff.Mode.MULTIPLY);
                        if(right){
                            if(tapAverageRight == 0){
                                tapAverageRight = tapCount;
                            }else {
                                tapAverageRight = (tapAverageRight + tapCount) / 2;
                            }
                        }else{
                            if(tapAverageRight == 0) {
                                tapAverageLeft = tapCount;
                            }else {
                                tapAverageLeft = (tapAverageLeft + tapCount) / 2;
                            }
                        }
                        final Handler h = new Handler();
                        h.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                tapButton.setEnabled(true);
                                tapButton.getBackground().clearColorFilter();
                            }
                        }, 2000);
                        tapButton.setText("Tap to start!");
                        tapCount = 0;
                        time_left_temp = time_left;
                        if(tempTrialAmount > 1){
                            tempTrialAmount -= 1;
                            runGame();
                        }else{
                            if(runOtherHand){
                                runOtherHand = false;
                                right = !right;
                                tempTrialAmount = trialAmount;
                                runGame();
                            }else{
                                save();
                                displayAlert();
                                //save
                                tempTrialAmount = trialAmount;
                                right = !right;
                                tapAverageLeft = 0;
                                tapAverageRight = 0;
                                runOtherHand = true;
                            }
                        }

                    } else if (time_left_temp > time_left - 5){
                        if(right && time_left_temp == time_left){
                            rightHand.setText("R");
                            leftHand.setText("");
                        }else if(!right && time_left_temp == time_left) {
                            rightHand.setText("");
                            leftHand.setText("L");
                        }
                        tapButton.getBackground().setColorFilter(0x88880000, PorterDuff.Mode.MULTIPLY);
                        tapButton.setText("Beginning in "+String.valueOf(time_left_temp-(time_left-5))+" seconds");
                        time_left_temp -= 1;
                        final Handler h = new Handler();
                        h.postDelayed(this, 1000);
                    } else {
                        tapButton.getBackground().setColorFilter(Color.parseColor("#0099cc"),PorterDuff.Mode.MULTIPLY);
                        tapButton.setText("Tap");
                        timer_text.setText(String.valueOf(time_left_temp) + " seconds");
                        time_left_temp -= 1;
                        final Handler h = new Handler();
                        h.postDelayed(this, 1000);
                    }
                }
            };

            task.run();
        }

        tapCount += 1;
    }

    private void displayAlert(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Left hand average: " + String.valueOf(tapAverageLeft)+"\n"
                            +"Right hand average: " + String.valueOf(tapAverageRight)+"\n" );
        builder1.setCancelable(true);
        builder1.show();
        sheet.writeData(CMSC436Sheet.TestType.LH_TAP, "t03p03", tapAverageLeft);
        sheet.writeData(CMSC436Sheet.TestType.RH_TAP, "t03p03", tapAverageRight);
    }

}
